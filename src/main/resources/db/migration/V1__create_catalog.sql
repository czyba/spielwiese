CREATE TABLE "catalog" (
    id VARCHAR(36) PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(1000),
    price INTEGER NOT NULL,
    stock INTEGER
);
