package de.czyba.spielwiese.application

import de.czyba.spielwiese.domain.ValidationError
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandler {

    @ExceptionHandler(ValidationError::class)
    fun handleValidationError(ex: ValidationError) : ResponseEntity<ErrorDTO> {
        val error = ErrorDTO(
            message = ex.message
        )
        return ResponseEntity(error, HttpStatus.BAD_REQUEST)
    }
}
