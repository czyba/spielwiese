package de.czyba.spielwiese.application.catalog

import de.czyba.spielwiese.domain.catalog.CatalogItem
import java.util.UUID

fun CatalogItem.toDTO() : GETCatalogItemDTO {
    return GETCatalogItemDTO(
            id = this.id.toString(),
            name = this.name,
            description = this.description,
            price = this.price,
            stock = this.stock,
    )
}

fun POSTCatalogItemDTO.toDomain(): CatalogItem {
    return CatalogItem(
        id = UUID.randomUUID(),
        name = name,
        price = price,
        description = description,
        stock = null,
    )
}

fun List<CatalogItem>.toDTO() : List<GETCatalogItemDTO> {
    return this.map(CatalogItem::toDTO)
}


