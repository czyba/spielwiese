package de.czyba.spielwiese.application.catalog

import java.math.BigDecimal

class POSTCatalogItemDTO(
    val name: String,
    val price: BigDecimal,
    val description: String?,
)
