package de.czyba.spielwiese.application.catalog

import java.math.BigDecimal

data class GETCatalogItemDTO(
        val id: String,
        val name: String,
        val price: BigDecimal,
        val description: String?,
        val stock: Int?,
)
