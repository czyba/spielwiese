package de.czyba.spielwiese.application.catalog

import de.czyba.spielwiese.domain.catalog.CatalogService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@RestController
class CatalogController(
        val catalogService: CatalogService
) {

    @GetMapping("/catalog", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCatalog(
        @RequestParam(required = false) cursor: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ): List<GETCatalogItemDTO> {
        return catalogService
            .getCatalog()
            .toDTO()
    }

    @PostMapping("/catalog", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun postCatalogItem(
        @RequestBody body: POSTCatalogItemDTO
    ) : ResponseEntity<Unit> {
        val catalogItem = body.toDomain()
        catalogService.createCatalogItem(catalogItem)
        return ResponseEntity.created(URI.create("/catalog/${catalogItem.id}")).build()
    }
}