package de.czyba.spielwiese.application.catalog

import de.czyba.spielwiese.domain.catalog.CatalogItemPublisher
import de.czyba.spielwiese.domain.catalog.CatalogRepository
import de.czyba.spielwiese.domain.catalog.CatalogService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CatalogConfiguration {

    @Bean
    fun catalogService(
        catalogRepository: CatalogRepository,
        catalogItemPublisher: CatalogItemPublisher,
    ) : CatalogService {
        return CatalogService(
            catalogRepository = catalogRepository,
            catalogItemPublisher = catalogItemPublisher,
        )
    }
}
