package de.czyba.spielwiese

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["de.czyba.spielwiese"])
class SpielwieseApplication

fun main(args: Array<String>) {
	runApplication<SpielwieseApplication>(*args)
}
