package de.czyba.spielwiese.domain

open class ValidationError(
    message: String
) : RuntimeException(message) {
    override val message: String
        get() = super.message!!
}