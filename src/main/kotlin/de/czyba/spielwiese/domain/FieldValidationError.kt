package de.czyba.spielwiese.domain

class FieldValidationError(
    fieldName: String,
    validationMessage: String,
) : ValidationError("Field '$fieldName' $validationMessage")