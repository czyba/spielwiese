package de.czyba.spielwiese.domain.catalog

class CatalogService(
    private val catalogRepository: CatalogRepository,
    private val catalogItemPublisher: CatalogItemPublisher
) {

    fun getCatalog(): List<CatalogItem> {
        return catalogRepository.getAll()
    }

    fun createCatalogItem(catalogItem: CatalogItem) {
        catalogRepository.storeItem(catalogItem)
        catalogItemPublisher.publishNewItem(catalogItem)
    }

}
