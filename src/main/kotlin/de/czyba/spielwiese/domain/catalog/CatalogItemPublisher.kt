package de.czyba.spielwiese.domain.catalog

interface CatalogItemPublisher {

    fun publishNewItem(item: CatalogItem)

}
