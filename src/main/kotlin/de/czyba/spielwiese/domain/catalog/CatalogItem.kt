package de.czyba.spielwiese.domain.catalog

import de.czyba.spielwiese.domain.FieldValidationError
import java.math.BigDecimal
import java.util.UUID

data class CatalogItem(
        val id: UUID,
        val name: String,
        val price: BigDecimal,
        val description: String?,
        val stock: Int?,
) {
    init {
        validate()
    }

    private fun validate() {
        priceMustNotBeLessOrEqualToZero()
        nameMustNotBeLongerThan100Chars()
        descriptionMustNotBeLongerThan1000Chars()
    }

    private fun priceMustNotBeLessOrEqualToZero() {
        if (price <= BigDecimal.ZERO) {
            throw FieldValidationError(
                fieldName = "price",
                validationMessage = "is less or equal to 0."
            )
        }
    }

    private fun descriptionMustNotBeLongerThan1000Chars() {
        if (description != null && description.length > 1000) {
            throw FieldValidationError(
                fieldName = "description",
                validationMessage = "is longer than 1000 characters."
            )
        }
    }

    private fun nameMustNotBeLongerThan100Chars() {
        if (name.length > 100) {
            throw FieldValidationError(
                fieldName = "name",
                validationMessage = "is longer than 100 characters."
            )
        }
    }
}