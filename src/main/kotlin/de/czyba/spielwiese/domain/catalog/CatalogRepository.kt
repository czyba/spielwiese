package de.czyba.spielwiese.domain.catalog

interface CatalogRepository {

    fun getAll() : List<CatalogItem>
    fun storeItem(catalogItem: CatalogItem)
}