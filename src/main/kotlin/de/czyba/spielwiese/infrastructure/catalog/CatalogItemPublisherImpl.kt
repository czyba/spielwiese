package de.czyba.spielwiese.infrastructure.catalog

import de.czyba.spielwiese.domain.catalog.CatalogItem
import de.czyba.spielwiese.domain.catalog.CatalogItemPublisher
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component

@Component
class CatalogItemPublisherImpl(
    val applicationEventPublisher: ApplicationEventPublisher
) : CatalogItemPublisher {

    override fun publishNewItem(item: CatalogItem) {
        val publishDTO = item.toPublishDTO()
        applicationEventPublisher.publishEvent(publishDTO)
    }

}
