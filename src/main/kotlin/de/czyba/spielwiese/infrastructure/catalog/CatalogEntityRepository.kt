package de.czyba.spielwiese.infrastructure.catalog

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CatalogEntityRepository : JpaRepository<CatalogItemEntity, String>