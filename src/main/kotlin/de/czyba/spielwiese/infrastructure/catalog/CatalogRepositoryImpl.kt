package de.czyba.spielwiese.infrastructure.catalog

import com.querydsl.jpa.impl.JPAQuery
import de.czyba.spielwiese.domain.catalog.CatalogItem
import de.czyba.spielwiese.domain.catalog.CatalogRepository
import jakarta.persistence.EntityManager
import org.springframework.stereotype.Repository

@Repository
class CatalogRepositoryImpl(
        private val catalogEntityRepository: CatalogEntityRepository,
        private val entityManager: EntityManager,
) : CatalogRepository {

    companion object {
        private val qCatalogItem : QCatalogItemEntity = QCatalogItemEntity.catalogItemEntity
    }

    override fun getAll(): List<CatalogItem> {
        val query = JPAQuery<Any>(entityManager)
            .from(qCatalogItem)
            .select(qCatalogItem);

        return fetchItemsInAscendingOrder(query);
    }

    private fun fetchItemsInAscendingOrder(query: JPAQuery<CatalogItemEntity>) =
            query.fetch()
                    .map(CatalogItemEntity::toDomain)
                    .sortedWith(Comparator.comparing { it.id.toString() })

    override fun storeItem(catalogItem: CatalogItem) {
        val entity = catalogItem.toEntity()
        catalogEntityRepository.save(entity)
    }

}