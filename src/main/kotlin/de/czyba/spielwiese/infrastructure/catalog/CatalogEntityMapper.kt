package de.czyba.spielwiese.infrastructure.catalog

import de.czyba.spielwiese.domain.catalog.CatalogItem
import java.util.UUID

fun CatalogItemEntity.toDomain() : CatalogItem {
    return CatalogItem(
            id = UUID.fromString(this.id),
            name = this.name,
            description = this.description,
            price = this.priceAsBigDecimal,
            stock = this.stock,
    )
}

fun CatalogItem.toEntity() : CatalogItemEntity {
    return CatalogItemEntity(
        id = this.id.toString(),
        name = this.name,
        description = this.description,
        price = this.price.setScale(2).toLong(),
        stock = this.stock,
    )
}
