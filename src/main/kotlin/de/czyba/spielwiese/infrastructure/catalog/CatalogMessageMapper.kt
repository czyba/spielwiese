package de.czyba.spielwiese.infrastructure.catalog

import de.czyba.spielwiese.domain.catalog.CatalogItem
import de.czyba.spielwiese.infrastructure.NewCatalogItemPublishDTO

fun CatalogItem.toPublishDTO() : NewCatalogItemPublishDTO {
    return NewCatalogItemPublishDTO(
        id = this.id,
        name = this.name,
        description = this.description,
        price = this.price,
    )
}
