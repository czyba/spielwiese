package de.czyba.spielwiese.infrastructure.catalog

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.math.BigDecimal

@Entity
@Table(name = "catalog")
class CatalogItemEntity(
        @Id var id: String,
        var name: String,
        var description: String?,
        private var price: Long,
        var stock: Int?
) {
    val priceAsBigDecimal : BigDecimal
        get() = BigDecimal.valueOf(this.price, 2)
}