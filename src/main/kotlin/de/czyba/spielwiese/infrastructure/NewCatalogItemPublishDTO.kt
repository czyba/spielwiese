package de.czyba.spielwiese.infrastructure

import java.math.BigDecimal
import java.util.UUID

class NewCatalogItemPublishDTO(
    val id: UUID,
    val name: String,
    val price: BigDecimal,
    val description: String?,
)
