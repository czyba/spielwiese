package de.czyba.spielwiese

import org.springframework.context.annotation.Configuration

@Configuration
class TestcontainerConfiguration {
    companion object {
        val container : PostgresSQLContainer = PostgresSQLContainer.container
    }
}