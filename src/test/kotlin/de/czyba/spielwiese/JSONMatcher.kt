package de.czyba.spielwiese

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.BaseMatcher
import org.hamcrest.Description

class JSONMatcher private constructor(private val expectedTree: JsonNode) : BaseMatcher<String>() {
    override fun describeTo(p0: Description) {
        p0.appendText(expectedTree.toString())
    }

    override fun matches(p0: Any?): Boolean {
        if (p0 == null) {
            return false
        }
        if (p0 !is String) {
            return false
        }
        val actualTree = objectMapper.readTree(p0)
        return actualTree == expectedTree
    }

    companion object {
        fun equalsJson(json: String) : JSONMatcher {
            val expectedTree = objectMapper.readTree(json)
            return JSONMatcher(expectedTree)
        }
        private val objectMapper: ObjectMapper = ObjectMapper()
    }
}