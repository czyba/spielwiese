package de.czyba.spielwiese

import org.testcontainers.containers.PostgreSQLContainer

class PostgresSQLContainer private constructor() : PostgreSQLContainer<PostgresSQLContainer?>(IMAGE_VERSION) {
    override fun start() {
        super.start()
        System.setProperty("TESTCONTAINERS_JDBC_URL", container.jdbcUrl)
        System.setProperty("TESTCONTAINERS_DB_USERNAME", container.username)
        System.setProperty("TESTCONTAINERS_DB_PASSWORD", container.password)
    }

    override fun stop() {
        //do nothing, JVM handles shut down
    }

    companion object {
        private const val IMAGE_VERSION = "postgres:15-alpine"
        val container: PostgresSQLContainer = PostgresSQLContainer()
        init {
            container.start()
        }
    }
}
