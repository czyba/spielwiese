package de.czyba.spielwiese.application.catalog

import de.czyba.spielwiese.JSONMatcher.Companion.equalsJson
import io.restassured.RestAssured.given
import io.restassured.builder.RequestSpecBuilder
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import java.math.BigDecimal
import java.util.UUID

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GetCatalogTests @Autowired constructor(
        val catalogTestService: CatalogTestService
) {

    @LocalServerPort
    var port: Int = 0

    lateinit var spec : RequestSpecification

    @BeforeEach
    fun setUp() {
        catalogTestService.clearCatalog()
        spec = RequestSpecBuilder()
                .setBasePath("/catalog")
                .setPort(port)
                .build()
    }

    @Test
    @DisplayName("GET /catalog with Accept: application/json should work")
    fun test_fetching_empty_data_with_accept_type_json_works() {
        given(spec)
                .accept(ContentType.JSON)
                .get()
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(equalsJson("""[]""".trimIndent()))
    }

    @Test
    @DisplayName("GET /catalog with Accept: */* should work")
    fun test_fetching_empty_data_with_accept_type_any_works() {
        given(spec)
                .accept(ContentType.ANY)
                .get()
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(equalsJson("""[]""".trimIndent()))
    }

    @Test
    @DisplayName("GET should return 1 item if 1 item is in database")
    fun test_fetching_one_item_works() {
        val item = catalogTestService.insertItem(
                name = "Hemd",
                price = BigDecimal.valueOf(2999, 2),
                description = "Ein schickes Hemd.",
                stock = 2,
        )

        given(spec)
                .accept(ContentType.JSON)
                .get()
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(equalsJson("""[{
                        "name": "Hemd",
                        "description": "Ein schickes Hemd.",
                        "price": 29.99,
                        "id": "${item.id}",
                        "stock": 2
                    }]""".trimIndent()))
    }

    @Test
    @DisplayName("GET should return 2 items in id order if 2 item is in database")
    fun test_fetching_two_items_ascending_works() {
        val uuidGroesser = UUID.fromString("ef142a49-bee1-4978-9a34-a2a4741c0d1f")
        val uuidKleiner = UUID.fromString("ef142a49-bee1-4978-9a34-a2a4741c0d1e")
        catalogTestService.insertItem(
            id = uuidGroesser,
            name = "Hemd",
            price = BigDecimal.valueOf(2999, 2),
            description = "Ein schickes Hemd.",
            stock = 2,
        )

        catalogTestService.insertItem(
            id = uuidKleiner,
            name = "Hemd",
            price = BigDecimal.valueOf(2999, 2),
            description = "Ein anderes schickes Hemd.",
            stock = 2,
        )

        given(spec)
            .accept(ContentType.JSON)
            .get()
            .then()
            .statusCode(200)
            .contentType(ContentType.JSON)
            .body(equalsJson("""[{
                        "name": "Hemd",
                        "description": "Ein anderes schickes Hemd.",
                        "price": 29.99,
                        "id": "$uuidKleiner",
                        "stock": 2
                    }, {
                        "name": "Hemd",
                        "description": "Ein schickes Hemd.",
                        "price": 29.99,
                        "id": "$uuidGroesser",
                        "stock": 2
                    }]""".trimIndent()))
    }

}