package de.czyba.spielwiese.application.catalog

import de.czyba.spielwiese.infrastructure.NewCatalogItemPublishDTO
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class TestCatalogItemListener() {

    val items: MutableList<NewCatalogItemPublishDTO> = ArrayList()

    fun reset() {
        items.clear()
    }

    @EventListener(NewCatalogItemPublishDTO::class)
    fun handlePublishEvent(event: NewCatalogItemPublishDTO) {
        items.add(event)
    }
}