package de.czyba.spielwiese.application.catalog

import de.czyba.spielwiese.JSONMatcher
import io.restassured.RestAssured.given
import io.restassured.builder.RequestSpecBuilder
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import org.hamcrest.Matchers
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import java.math.BigDecimal

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PostCatalogItemTests @Autowired constructor(
    val catalogTestService: CatalogTestService,
    val testCatalogItemListener: TestCatalogItemListener,
) {

    @LocalServerPort
    var port: Int = 0

    lateinit var spec : RequestSpecification

    @BeforeEach
    fun setUp() {
        catalogTestService.clearCatalog()
        spec = RequestSpecBuilder()
                .setBasePath("/catalog")
                .setPort(port)
                .build()
        testCatalogItemListener.reset()
    }

    @Test
    @DisplayName("POST /catalog with Content: application/json should work")
    fun test_creating_a_new_catalog_item_should_work() {
        given(spec)
                .contentType(ContentType.JSON)
                .body(
                    """
                    {
                        "name": "Hemd",
                        "description": "Ein schickes Hemd",
                        "price": 29.99
                    }
                    """.trimIndent()
                )
                .post()
                .then()
                .statusCode(201)
                .header("location", Matchers.matchesRegex("/catalog/[a-fA-Z0-9]{8}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{12}"))
    }

    @Test
    @DisplayName("POST /catalog without description should work")
    fun test_creating_a_new_catalog_item_without_description_should_work() {
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "Hemd",
                        "price": 29.99
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(201)
            .header("location", Matchers.matchesRegex("/catalog/[a-fA-Z0-9]{8}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{12}"))
    }

    @Test
    @DisplayName("POST /catalog without description should work")
    fun test_creating_a_new_catalog_item_with_values_at_the_edge_of_validity_should_work() {
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "${"a".repeat(100)}",
                        "description": "${"a".repeat(1000)}",
                        "price": 29.99
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(201)
            .header("location", Matchers.matchesRegex("/catalog/[a-fA-Z0-9]{8}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{12}"))
    }

    @Test
    @DisplayName("POST /catalog with Content: should publish message")
    fun test_creating_a_new_catalog_item_should_publish_message() {
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "Hemd",
                        "description": "Ein schickes Hemd",
                        "price": 29.99
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(201)
            .header("location", Matchers.matchesRegex("/catalog/[a-fA-Z0-9]{8}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{4}-[a-fA-Z0-9]{12}"))

        assertEquals(1, testCatalogItemListener.items.size)
        val item = testCatalogItemListener.items[0]
        assertEquals("Hemd", item.name)
        assertEquals("Ein schickes Hemd", item.description)
        assertEquals(BigDecimal.valueOf(2999, 2), item.price)
    }

    @Test
    @DisplayName("POST /catalog without name should not work")
    fun test_creating_a_new_catalog_item_without_name_should_work() {
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "description": "Ein schickes Hemd",
                        "price": 29.99
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(400)
    }

    @Test
    @DisplayName("POST /catalog with a too long name should not work")
    fun test_posting_a_catalog_item_with_a_too_long_name_should_return_400() {
        val name = "a".repeat(101)
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "$name",
                        "description": "Ein schickes Hemd",
                        "price": 29.99
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(400)
            .body(
                JSONMatcher.equalsJson(
                    """{
                        "message": "Field 'name' is longer than 100 characters."
                    }"""
                ))
    }

    @Test
    @DisplayName("POST /catalog without price should not work")
    fun test_creating_a_new_catalog_item_without_price_should_not_work() {
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "Hemd",
                        "description": "Ein schickes Hemd"
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(400)
    }

    @Test
    @DisplayName("POST /catalog with a too long description should not work")
    fun test_posting_a_catalog_item_with_a_too_long_description_should_return_400() {
        val description = "a".repeat(1001)
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "Hemd",
                        "description": "$description",
                        "price": 29.99
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(400)
            .body(
                JSONMatcher.equalsJson(
                    """{
                        "message": "Field 'description' is longer than 1000 characters."
                    }"""
                ))
    }

    @Test
    @DisplayName("POST /catalog with a price lower than 0 should not work")
    fun test_posting_a_catalog_item_with_a_price_lower_than_0_should_return_400() {
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "Hemd",
                        "description": "Ein schickes Hemd",
                        "price": -1.23
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(400)
            .body(
                JSONMatcher.equalsJson(
                    """{
                        "message": "Field 'price' is less or equal to 0."
                    }"""
                ))
    }

    @Test
    @DisplayName("POST /catalog with a price equal to 0 should not work")
    fun test_posting_a_catalog_item_with_a_price_equal_to_0_should_return_400() {
        given(spec)
            .contentType(ContentType.JSON)
            .body(
                """
                    {
                        "name": "Hemd",
                        "description": "Ein schickes Hemd",
                        "price": 0
                    }
                    """.trimIndent()
            )
            .post()
            .then()
            .statusCode(400)
            .body(
                JSONMatcher.equalsJson(
                    """{
                        "message": "Field 'price' is less or equal to 0."
                    }"""
                ))
    }

}