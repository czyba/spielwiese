package de.czyba.spielwiese.application.catalog

import de.czyba.spielwiese.domain.catalog.CatalogItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.UUID


@Component
class CatalogTestService {
    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    fun insertItem(
            id: UUID = UUID.randomUUID(),
            name: String,
            description: String? = null,
            price: BigDecimal = BigDecimal.ONE,
            stock: Int? = null
    ) : CatalogItem {
        val sql = "INSERT INTO catalog (id, name, description, price, stock) VALUES (?, ?, ?, ?, ?)"
        jdbcTemplate.update(sql, id.toString(), name, description, price.setScale(2).unscaledValue(), stock)
        return CatalogItem(
                id = id,
                name = name,
                description = description,
                price = price,
                stock = stock,
        )
    }

    fun insertItems(count: Int): List<CatalogItem> {
        return (0 until count).map {
            insertItem(
                    name = "Item-${it}",
                    description = "Description-${it}",
                    price = BigDecimal.valueOf((1 + it) * 100L + ((1 + it) % 100), 2),
                    stock = it
            )
        }
    }

    fun clearCatalog() {
        jdbcTemplate.execute("TRUNCATE TABLE catalog;")
    }
}