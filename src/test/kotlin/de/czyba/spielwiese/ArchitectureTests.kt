package de.czyba.spielwiese

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*
import org.junit.jupiter.api.DisplayName
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.*


@AnalyzeClasses(packages = ["de.czyba.spielwiese.."])
class ArchitectureTests {

    @ArchTest
    @DisplayName("Get, Post, Put, Delete methods must reside in controllers.")
    fun test_rest_methods_must_reside_in_controllers_rule(importedClasses: JavaClasses) {
        val rule = methods()
                .that()
                .areAnnotatedWith(GetMapping::class.java)
                .or()
                .areAnnotatedWith(PostMapping::class.java)
                .or()
                .areAnnotatedWith(PutMapping::class.java)
                .or()
                .areAnnotatedWith(DeleteMapping::class.java)
                .should()
                .beDeclaredInClassesThat()
                .resideInAPackage("de.czyba.spielwiese.application..")
                .andShould()
                .beDeclaredInClassesThat()
                .areAnnotatedWith(RestController::class.java)
                .andShould()
                .beDeclaredInClassesThat()
                .haveSimpleNameEndingWith("Controller")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("There should be no PATCH methods.")
    fun test_rest_methods_must_not_be_head_option_or_patch(importedClasses: JavaClasses) {
        val rule = noMethods()
                .should()
                .beAnnotatedWith(PatchMapping::class.java)

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Controllers may only reside in the application sub-package.")
    fun test_controllers_only_residing_in_application(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .haveSimpleNameEndingWith("Controller")
                .should()
                .resideInAPackage("de.czyba.spielwiese.application..")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Classes residing in the catalog package may only be used by other classes inside the catalog package.")
    fun classes_inside_catalog_package_may_only_be_accessed_by_other_classes_in_catalog(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .resideInAPackage("..catalog..")
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAPackage("..catalog..")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Classes residing in the domain package may only use plain java.")
    fun classes_residing_in_domain_may_only_be_plain_java(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .resideInAPackage("..domain..")
                .should()
                .dependOnClassesThat()
                .resideInAnyPackage("..domain..", "java.lang")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Classes residing in infrastructure may only be accessed by infrastructure or application")
    fun classes_residing_in_infrastructure_may_only_be_accessed_by_infrastructure_or_application(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .resideInAPackage("..infrastructure..")
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage("..infrastructure..", "..application..")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Classes residing in application may only be accessed by application")
    fun classes_residing_in_application_may_only_be_accessed_by_application(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .resideInAPackage("..application..")
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage("..application..")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Services should only reside in domain")
    fun service_should_only_reside_in_domain(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .haveSimpleNameEndingWith("Service")
                .and()
                .haveSimpleNameNotEndingWith("TestService")
                .should()
                .resideInAPackage("..domain..")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Repositories should only reside in infrastructure")
    fun jpa_repositories_should_only_reside_in_infrastructure(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .haveSimpleNameEndingWith("Repository")
                .and()
                .areAssignableTo(JpaRepository::class.java)
                .should()
                .resideInAPackage("..infrastructure..")

        rule.check(importedClasses)
    }

    @ArchTest
    @DisplayName("Repositories should be annotated with Repository")
    fun jpa_repositories_should_should_be_annotated_with_repository(importedClasses: JavaClasses) {
        val rule = classes()
                .that()
                .haveSimpleNameEndingWith("Repository")
                .and()
                .areAssignableTo(JpaRepository::class.java)
                .should()
                .beAnnotatedWith(Repository::class.java)

        rule.check(importedClasses)
    }
}